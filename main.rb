#!/usr/bin/env ruby

def eval_bracket(rule, content, dir)
	if dir == 0
		res = rule.left.scan /(\!.)/
		calc_not(rule, res, content, dir) if rule.left =~ /(\!.)/

		res = rule.left.scan /(..\+..)/
		calc_and(rule, res, content, dir) if rule.left =~ /(..\+..)/

		res = rule.left.scan /(..\|..)/
		calc_or(rule, res, content, dir) if rule.left =~ /(..\|..)/

		res = rule.left.scan /(..\^..)/
		calc_xor(rule, res, content, dir) if rule.left =~ /(..\^..)/
	else
		res = rule.right.scan /(\!.)/
		calc_not(rule, res, content, dir) if rule.right =~ /(\!.)/

		res = rule.right.scan /(..\+..)/
		calc_and(rule, res, content, dir) if rule.right =~ /(..\+..)/

		res = rule.right.scan /(..\|..)/
		calc_or(rule, res, content, dir) if rule.right =~ /(..\|..)/

		res = rule.right.scan /(..\^..)/
		calc_xor(rule, res, content, dir) if rule.right =~ /(..\^..)/
	end
end

def calc_bracket(line, result, content, dir)
	tmp = get_rule(result[0][0].dup, line.sign, line.right.dup)
	if dir == 0
		tmp.left[0] = ''
		tmp.left[tmp.left.length - 1] = ''
	else
		tmp.right[0] = ''
		tmp.right[tmp.right.length - 1] = ''
	end

	eval_bracket(tmp, content, dir)
	if dir == 0
		line.left["#{result[0][0]}"] = tmp.left
		return if $g_error == 1
		res = line.left.scan /(\([^\)\(]*\))/
		calc_bracket(line, res, content, dir) if line.left =~ /(\([^\)\(]*\))/
	else
		line.right["#{result[0][0]}"] = tmp.right
		return if $g_error == 1
		res = line.right.scan /(\([^\)\(]*\))/
		calc_bracket(line, res, content, dir) if line.right =~ /(\([^\)\(]*\))/
	end
end

def calc_not(line, result, content, dir) # dir == 0 for left, 1 for right
	i = 0
	value = 0
	max = result.size
	while i < max
		search = result[i][0][0] + "([" + result[i][0][1] + "])"
		value = 1 if get_letter(content, result[i][0][1]) == 0
		if dir == 0
			line.left = line.left.gsub(/#{search}/, value.to_s)
		else
			line.right = line.right.gsub(/#{search}/, value.to_s)
		end
		i += 1
	end
end

def calc_and(line, result, content, dir)
	i = 0
	max = result.size
	while i < max
		if (result[i][0][0] == '0' || get_letter(content, result[i][0][0]) == 0) && (result[i][0][4] == '0' || get_letter(content, result[i][0][4]) == 0)
			if dir == 0
				line.left["#{result[i][0]}"] = "0"
			else
				line.right["#{result[i][0]}"] = "0"
			end
		else
			if dir == 0
				line.left["#{result[i][0]}"] = "1"
			else
				line.right["#{result[i][0]}"] = "1"
			end
		end
		i += 1
	end
	if dir == 0
		res = line.left.scan /(..\+..)/
		calc_and(line, res, content, dir) if line.left =~ /(..\+..)/
	else
		res = line.right.scan /(..\+..)/
		calc_and(line, res, content, dir) if line.right =~ /(..\+..)/
	end
end

def calc_or(line, result, content, dir)
	i = 0
	max = result.size
	while i < max
		# line.left = line.left.gsub(/#{result[i][0][0]}/, get_letter(content, result[i][0][0]).to_s) if (dir == 0 && result[i][0][0] >= 'A' && result[i][0][0] <= 'Z')
		# line.left = line.left.gsub(/#{result[i][0][4]}/, get_letter(content, result[i][0][4]).to_s) if (dir == 0 && result[i][0][4] >= 'A' && result[i][0][4] <= 'Z')
		if (result[i][0][0] == '0' || get_letter(content, result[i][0][0]) == 0) || (result[i][0][4] == '0' || get_letter(content, result[i][0][4]) == 0)
			if dir == 0
				line.left["#{result[i][0]}"] = "0"
			else
				line.right["#{result[i][0]}"] = "0"
			end
		else
			if dir == 0
				line.left["#{result[i][0]}"] = "1"
			else
				line.right["#{result[i][0]}"] = "1"
			end
		end
		i += 1
	end
	if dir == 0
		res = line.left.scan /(..\|..)/
		calc_and(line, res, content, dir) if line.left =~ /(..\|..)/
	else
		res = line.right.scan /(..\|..)/
		calc_and(line, res, content, dir) if line.right =~ /(..\|..)/
	end
end

def calc_xor(line, result, content, dir)
	i = 0
	max = result.size
	while i < max
		one = 0
		one = 1 if (result[i][0][0] == '1' || get_letter(content, result[i][0][0]) == 1)
		two = 0
		two = 1 if (result[i][0][4] == '1' || get_letter(content, result[i][0][4]) == 1)
		if (one == 0 && two == 1) || (one == 1 && two == 0)
			if dir == 0
				line.left["#{result[i][0]}"] = "0"
			else
				line.right["#{result[i][0]}"] = "0"
			end
		else
			if dir == 0
				line.left["#{result[i][0]}"] = "1"
			else
				line.right["#{result[i][0]}"] = "1"
			end
		end
		i += 1
	end
	if dir == 0
		res = line.left.scan /(..\^..)/
		calc_and(line, res, content, dir) if line.left =~ /(..\^..)/
	else
		res = line.right.scan /(..\^..)/
		calc_and(line, res, content, dir) if line.right =~ /(..\^..)/
	end
end

def replace_letter(str, content)
	i = 0
	while i < str.length
		if str[i] >= 'A' && str[i] <= 'Z'
			str[i] = get_letter(content, str[i]).to_s
		end
		i += 1
	end
end

def rule_answer(rule, content, origin_rule)
	rule.left = rule.left.gsub(/[A-Z]/) {|s| get_letter(content, s).to_s }
	result = 1
	if rule.sign == 0
		result = 0 if rule.left.include? ?0
		# result = 1 if rule.left.include? ?1
		i = 0
		while i < rule.right.length
			if rule.right[i] >= 'A' && rule.right[i] <= 'Z'
				if (result == 0)
					result = 1 if rule.right[i - 1] == '!'
					if get_letter(content, rule.right[i]) == 1 && result == 0
						set_letter(content, rule.right[i], 0)
						puts "Eval rule: " + origin_rule.left + (origin_rule.sign == 0 && "=>" or "<=>") + origin_rule.right
						puts "Result: letter: " + rule.right[i] + " = 0" + "\n "
						$g_change = 1
					elsif get_letter(content, rule.right[i]) == 0 && result == 1
						$g_error = 1
					end 
				end
			end
			i += 1
		end
	else
		replace_letter(rule.left, content)
		replace_letter(rule.right, content)
		result_l = 1
		result_l = 0 if rule.left.include? ?0
		result_r = 1
		result_r = 0 if rule.right.include? ?0
		$g_error = 1 if result_l != result_r
	end
end

def eval_rule(rule, content, origin_rule)
	# puts "===== eval rule ==== ", rule
	res = rule.left.scan /(\([^\)\(]*\))/
	calc_bracket(rule, res, content, 0) if rule.left =~ /(\([^\)\(]*\))/

	res = rule.left.scan /(\!.)/
	calc_not(rule, res, content, 0) if rule.left =~ /(\!.)/

	res = rule.left.scan /(..\+..)/
	calc_and(rule, res, content, 0) if rule.left =~ /(..\+..)/

	res = rule.left.scan /(..\|..)/
	calc_or(rule, res, content, 0) if rule.left =~ /(..\|..)/

	res = rule.left.scan /(..\^..)/
	calc_xor(rule, res, content, 0) if rule.left =~ /(..\^..)/

	if rule.sign == 1
		res = rule.right.scan /(\([^\)\(]*\))/
		calc_bracket(rule, res, content, 1) if rule.right =~ /(\([^\)\(]*\))/

		res = rule.right.scan /(\!.)/
		calc_not(rule, res, content, 1) if rule.right =~ /(\!.)/

		res = rule.right.scan /(..\+..)/
		calc_and(rule, res, content, 1) if rule.right =~ /(..\+..)/

		res = rule.right.scan /(..\|..)/
		calc_or(rule, res, content, 1) if rule.right =~ /(..\|..)/

		res = rule.right.scan /(..\^..)/
		calc_xor(rule, res, content, 1) if rule.right =~ /(..\^..)/
	end

	rule_answer(rule, content, origin_rule)
end

def search_letter(content, search)
	exist = false
	content.each do |letter|
		exist = true if letter.letter == search
	end

	exist
end

def get_letter(content, search)
	content.each do |letter|
		return letter.status if letter.letter == search
	end
end

def set_letter(content, search, status)
	content.each do |letter|
		if letter.letter == search
			letter.status = status
			return
		end
	end
end

def prs_get_letter(line, content)
	i = 0
	while i < line.length && line[i] != '#' && line[0] != '='
		if line[i] >= 'A' && line[i] <= 'Z'
			content.insert(-1, get_state(line[i], 1)) if search_letter(content, line[i]) == false
		elsif line[i] != ' ' && line[i] != '+' && line[i] != '|' && line[i] != '^' && line[i] != '!' && line[i] != '(' && line[i] != ')' && line[i] != '<' && line[i] != '>' && line[i] != '?' && line[i] != '=' && line[i].ord != 10
			puts_error("(prs_get_letter) Symbol Error : #{line[i].ord} [#{line[i]}]")
			return
		end
		i += 1
	end

	while i < line.length && line[i] != '#' && line[0] == '='
		if line[i] >= 'A' && line[i] <= 'Z'
			if search_letter(content, line[i]) == false
				content.insert(-1, get_state(line[i], 0))
			else
				set_letter(content, line[i], 0)
			end
		end
		i += 1
	end
end

def check_rule_syntax(str)
	i = 0
	state = 0
	while i < str.length
		if state == 0
			while i < str.length && (str[i] < 'A' || str[i] > 'Z')
				return false if str[i] == '+' || str[i] == '|' || str[i] == '^'
				i += 1
			end
			state = 1 if i < str.length && str[i] >= 'A' && str[i] <= 'Z'
		else
			while i < str.length && str[i] != '+' && str[i] != '|' && str[i] != '^'
				return false if str[i] >= 'A' && str[i] <= 'Z'
				i += 1
			end
			state = 0 if i < str.length && str[i] == '+' || str[i] == '|' || str[i] == '^'
		end
		i += 1
	end
	return false if state == 0

	true
end

def prs_get_rules(line, rules, question)
	if line[0] == "#"
		return
	end
	line.delete!("\n")
	tmp = line.split("#")
	if tmp[0].include? "<=>"
		if tmp[0].include? "|" or tmp[0].include? "^"
			puts "Ignoring line: " + line
		else
			part = tmp[0].split("<=>")
			if part.length != 2
				puts_error("(prs_get_rules) Error in: #{line}")
			else
				rules.insert(-1, get_rule(part[0], 1, part[1]))
			end
		end
		
	elsif tmp[0].include? "=>"
		part = tmp[0].split("=>")
		if part.length != 2
			puts_error("(prs_get_rules) Error in: #{line}")
			return
		end
		if part[1].include? "|"
			puts "Ignoring line: " + line
		elsif part[1].include? "^"
			puts "Ignoring line: " + line
		else
			if check_rule_syntax(part[0]) == true && check_rule_syntax(part[1]) == true
				rules.insert(-1, get_rule(part[0], 0, part[1]))
			else
				puts_error("(prs_get_rules) Error in: #{line}")
			end
		end
	elsif line[0] == '?'
		i = 1
		while i < line.length && line[i] != '#'
			if line[i] >= 'A' && line[i] <= 'Z'
				question.insert(-1, line[i])
			elsif line[i] != ' '
				puts_error("(prs_get_rules) Error in: #{line}")
			end
			i += 1
		end
	elsif line[0] != '='
		puts_error("(prs_get_rules) Error in: #{line}")
	end
end

def get_state(letter, status)
	state = Struct.new(:letter, :status) # status : 0, 1 (true, false)
	
	state.new(letter, status)
end

def get_rule(left, sign, right)
	rule = Struct.new(:left, :sign, :right) # sign : 0 or 1 (=> or  <=>)
	
	rule.new(left, sign, right)
end

def puts_error(str)
	puts "Expert-System:: " + str + "\n "
	$g_error = 1
end

def check_data(content, rules, question)
	if content.empty? || rules.empty? || question.empty?
		puts "Expert-System:: Error: " + (content.empty? && "no fact" or rules.empty? && "no rules" or "no question")
		return false
	end

	true
end

def eval_file(filename)
	$g_change = 1
	$g_error = 0
	content = Array.new
	question = Array.new
	rules = Array.new
	File.readlines(filename).each do |line|
		prs_get_letter(line, content)
		return if $g_error == 1
		prs_get_rules(line, rules, question)
		return if $g_error == 1
	end
	return if check_data(content, rules, question) == false
	content = content.sort { |x, y| x.letter <=> y.letter }
	question = question.sort { |x, y| x <=> y }
	puts "Starting fact:"
	content.each {|x| puts("#{x.letter} =  #{x.status}") }
	puts "\nRules:"
	rules.each {|x| puts(x.left + (x.sign == 0 && "=>" or "<=>") + x.right) }
	puts "\nQuestion:"
	question.each {|x| puts(x + " = ?") }

	puts "\nEvaluation: "
	while $g_change == 1
		$g_change = 0
		for rule in rules
			cpy = get_rule(rule.left.dup, rule.sign, rule.right.dup)
			eval_rule(cpy, content, rule)
			if $g_error == 1
				puts "Expert-System:: Error: incoherence in line " + rule.left + (rule.sign == 0 && "=>" or "<=>") + rule.right
				return
			end
		end
	end
	puts "Ending fact:"
	content.each {|x| puts("#{x.letter} =  #{x.status}") }
	puts "\nAnswers:"
	question.each {|x| puts(x + " = " + (get_letter(content, x) == 0 && "true" or "false")) }
end

def main()
	if ARGV.empty?
		puts "Expert-System:: ./main.rb filename"
	else
		ARGV.each do |filename|
			puts "\n=> Eval " + filename + "\n "
			if File.exist?(filename) && File.file?(filename)
				eval_file(filename)
			else
				puts "Expert-System:: Error: No such file: " + filename + "\n "
			end
		end
	end
end

$g_change = 1
$g_error = 0
main()
